package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

const (
	flagFile           = ".runonce"
	dockerDebugEnvName = "DOCKER_DEBUG"
	kanikoDockerPath   = "/kaniko/.docker/"
	buildArgBase       = "%s=%s"
)

// Build the complete Kaniko command according to the env vars
func buildKanikoParameters(options map[string]*string, buildArgs map[string]string) []string {
	// Kaniko build parameters
	buildMapParameters := []string{}
	verbosity := "info"

	// Exit if any of the mandatory options is missing
	if options["CONTEXT_DIR"] == nil || options["DOCKER_FILE"] == nil || options["TO"] == nil {
		log.Fatal("Kaniko configuration is invalid")
	}

	// Default verbosity is info for backwards compatibility
	if options["DOCKER_DEBUG"] != nil {
		if *options["DOCKER_DEBUG"] == "true" {
			verbosity = "debug"
		} else {
			verbosity = "info"
		}
	}

	buildMapParameters = append(buildMapParameters, "--context", *options["CONTEXT_DIR"])
	// DOCKER_FILE is relative to the context dir but Kaniko expected an absolute path
	dockerFilePath := path.Join(*options["CONTEXT_DIR"], *options["DOCKER_FILE"])
	buildMapParameters = append(buildMapParameters, "--dockerfile", dockerFilePath)
	buildMapParameters = append(buildMapParameters, "--destination", *options["TO"])
	buildMapParameters = append(buildMapParameters, "--verbosity=" + verbosity)

	// Add extra build args if they exist
	for k, v := range buildArgs {
		buildArg := fmt.Sprintf(buildArgBase, k, v)
		buildMapParameters = append(buildMapParameters, "--build-arg", buildArg)
	}
	log.Printf("Using the following build arguments: %v", buildMapParameters)

	return buildMapParameters
}


func prepareDockerConfig(options map[string]*string) error {
	dockerConfig := "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\"}}}"
	if options["DOCKER_LOGIN_SERVER"] == nil || options["DOCKER_LOGIN_USERNAME"] == nil || options["DOCKER_LOGIN_PASSWORD"] == nil {
		return fmt.Errorf("Docker configuration is invalid.")
	}

	dockerConfig = fmt.Sprintf(dockerConfig, *options["DOCKER_LOGIN_SERVER"], *options["DOCKER_LOGIN_USERNAME"], *options["DOCKER_LOGIN_PASSWORD"])

	// Precreate directories otherwise file creation fails
	os.MkdirAll(kanikoDockerPath, os.ModePerm)
	file, err := os.Create(path.Join(kanikoDockerPath, "config.json"))

    if err != nil {
        return err
    }
    defer file.Close()

	file.WriteString(dockerConfig)
	return nil
}

// obtainDefaults creates a map with the default values for all the variables
// that the docker build accepts
func obtainDefaults() map[string]*string {
	return map[string]*string{
		"FROM":                  nil,
		"TO":                    getEnvValue("CI_REGISTRY_IMAGE", nil),
		"CONTEXT_DIR":           getPointer("."),
		"DOCKER_FILE":           getPointer("Dockerfile"),
		"DOCKER_LOGIN_SERVER":   getEnvValue("CI_REGISTRY", nil),
		"DOCKER_LOGIN_USERNAME": getEnvValue("CI_REGISTRY_USER", nil),
		"DOCKER_LOGIN_PASSWORD": getEnvValue("CI_REGISTRY_PASSWORD", nil),
		"DOCKER_DEBUG":          getEnvValue("DOCKER_DEBUG", nil),
	}
}

// Retrieves a pointer to a string when envName is defined
func getEnvValue(envName string, defaultValue *string) *string {
	if val, present := os.LookupEnv(envName); present {
		return &val
	}
	return defaultValue
}

// Helper function to convert a string into a pointer of a string
func getPointer(s string) *string {
	return &s
}

// This function overrides the default values if an environment variable with the same name is defined
func parseEnvVariables(defaults map[string]*string) map[string]*string {
	options := make(map[string]*string)

	// For each possible variable, see if they are set as environment variable and override them from the
	// default list
	for envName, defaultValue := range defaults {
		if envValue, present := os.LookupEnv(envName); present {
			options[envName] = &envValue
		} else {
			options[envName] = defaultValue
		}
	}

	return options
}

// Obtain build args from environment variables starting with 'BUILD_ARG'
func parseBuildArgs() (map[string]string, error) {

	buildArgs := make(map[string]string)
	// os.Environ returns a copy of strings representing the environment,
	// in the form "key=value".
	for _, env := range os.Environ() {
		if strings.HasPrefix(env, "BUILD_ARG") {
			comps := strings.SplitN(env, "=", 3)
			if len(comps) != 3 {
				return buildArgs, fmt.Errorf("Build arg '%s' is malformed. The value should be a key value pair", env)
			}
			buildArgs[comps[1]] = comps[2]
		}
	}
	return buildArgs, nil
}

//Prints useful information for the build
func printDockerBuild(options map[string]*string) {
	if options["TO"] != nil {
		log.Println("Building Docker image:", *options["TO"])
	}
	if options["FROM"] != nil {
		log.Println("Overriding FROM with:", *options["FROM"])
	}
	if *options["CONTEXT_DIR"] != "." {
		log.Println("Using Context directory:", *options["CONTEXT_DIR"])
	}
	for _, env := range os.Environ() {
		if strings.HasPrefix(env, "BUILD_ARG") {
			log.Println("Using build args: ", env)
		}
	}
}

// Override the FROM field from the Dockerfile
// This directly edits the Dockerfile and leaves it changed
func overrideFrom(dockerfile, newFrom *string) error {
	if dockerfile != nil && newFrom != nil {

		log.Printf("Overriding FROM statement with '%s'\n", *newFrom)
		input, err := ioutil.ReadFile(*dockerfile)
		if err != nil {
			return err
		}

		lines := strings.Split(string(input), "\n")

		for i, line := range lines {
			//Override FROM
			if strings.HasPrefix(line, "FROM ") {
				lines[i] = fmt.Sprintf("FROM %s", *newFrom)
				break
			}
		}
		output := strings.Join(lines, "\n")
		err = ioutil.WriteFile(*dockerfile, []byte(output), 0644)
		if err != nil {
			return err
		}
	}
	return nil
}


func main() {

	// Abort if we've already run. This is needed in GitLab CI due to entrypoint being called
	// a second time in each job for after_script, see https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380
	if _, present := os.LookupEnv("CI"); present {
		if _, err := os.Stat(flagFile); err == nil {
			log.Println("[docker-image-builder] Build already done (workaround for https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1380)")
			os.Exit(0)
		} else {
			// Create file if it does not exist
			os.OpenFile(flagFile, os.O_CREATE, 0666)
		}
	}

	 // Obtain docker build variables based on defaults and environment variables
	options := parseEnvVariables(obtainDefaults())

	// When running inside gitlab-ci, combine the CONTEXT_DIR with wherever the probject is cloned
	if ciProjectDir, present := os.LookupEnv("CI_PROJECT_DIR"); present {
		options["CONTEXT_DIR"] = getPointer(path.Join(ciProjectDir, strings.TrimPrefix(*options["CONTEXT_DIR"], "/")))
	}

	// provide kaniko with an absolute path for the context
	if ! filepath.IsAbs(*options["CONTEXT_DIR"]) {
		absPath, err := filepath.Abs(*options["CONTEXT_DIR"])
		if err != nil {
			log.Fatal(err)
		}
		options["CONTEXT_DIR"] = getPointer(absPath)
	}

	printDockerBuild(options)
	// Obtain Build args
	buildArgs, err := parseBuildArgs()
	if err != nil {
		log.Fatal(err)
	}

	dockerfilepath := path.Join(*options["CONTEXT_DIR"], *options["DOCKER_FILE"])
	if err := overrideFrom(&dockerfilepath, options["FROM"]); err != nil {
		log.Fatal(err)
	}

	err = prepareDockerConfig(options)
	if err != nil {
		log.Fatal(err)
	}

	buildMapParameters := buildKanikoParameters(options, buildArgs)
	
	cmd := exec.Command("/kaniko/executor", buildMapParameters...)
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	os.Exit(0)
}
