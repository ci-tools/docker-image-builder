# Overview

> ⚠ Warning ⚠ This repo is deprecated. Please refer to [the upstream documentation](<https://docs.gitlab.com/ee/ci/docker/using_kaniko.html>).

A Docker image that can be used to build Docker images in GitLab-CI.

## Why this is needed

This project is a wrapper around [Kaniko](https://github.com/GoogleContainerTools/kaniko), which is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.

This software does not require access to the host's Docker daemon or having a privileged Docker-in-Docker service container, avoiding the use of dedicated GitLab shared runners with any [special configuration](http://docs.gitlab.com/ce/ci/docker/using_docker_build.html#runner-configuration).

For backwards compatibility this image not only allows to manually [run the Kaniko build](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html), it also allows the build without the need of running user-provided commands by using environment variables to control the build (previous requirement); this last method is considered **deprecated**.

**This image can run in normal default shared runners.**

You can as well use the upstream Docker image, but this image is the recommended option.

## Run a Docker-build job

In the past, the recommendeded method was to use `gitlab-registry.cern.ch/ci-tools/docker-image-builder` instead of `gcr.io/kaniko-project/executor:debug`. However, this `docker-image-builder` is now no longer maintained nor supported by the GitLab Team.

For guidelines on how to use this in your GitLab CI scripts, please take a look at [our corresponding examples](https://gitlab.cern.ch/gitlabci-examples/build_docker_image).

### Controlling the behavior of the build with env variables (deprecated method)

The following environment variables allow to control the behaviour of the Docker image build.

They are all optional:

`FROM`: a tag to be used to override the source Dockerfile's FROM line. This allows to build a same Dockerfile for multiple base images.

`TO`: a tag to apply to the resulting image. If specified, we'll try to push the image after the build. By default: `$CI_REGISTRY_IMAGE`. Check the information about [build variables](https://gitlab.cern.ch/help/ci/variables/README.md) for more details.

`CONTEXT_DIR`: folder where the Dockerfile to build is located; we'll run the docker build command in that folder. This enables building different Dockerfiles from a single repository (by default: repository root)

`DOCKER_FILE`: name of the Dockerfile (by default: 'Dockerfile')

`DOCKER_LOGIN_SERVER`: a docker registry to login into (allows to provide credentials for pushing the image). By default: gitlab-registry.cern.ch (`$CI_REGISTRY`). Check the information about [build variables](https://gitlab.cern.ch/help/ci/variables/README.md) for more details).

`DOCKER_LOGIN_USERNAME`: username for login into the docker registry (allows to provide credentials for pushing the image). By default: `gitlab-ci-token`.

`DOCKER_LOGIN_PASSWORD`: password for login into the docker registry (allows to provide credentials for pushing the image). By default `$CI_BUILD_TOKEN`.  Check the information about [build variables](https://gitlab.cern.ch/help/ci/variables/README.md) for more details.

`BUILD_ARG`: docker build arguments in key=value format. If several arguments are needed, provide `BUILD_ARG_1`, `BUILD_ARG_2` etc.

`NO_CACHE`: if set to any value, disable use of [the build cache](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#build-cache) (default: cache is enabled)

`DOCKER_DEBUG`: if set to "true", it will enable debug logging. Ignored otherwise.
